package com.example.tourismapp.event;

public class Events {
    public static class DialogErrorEvent {
        public final String title;
        public final String message;

        public DialogErrorEvent(String title, String message) {
            this.title = title;
            this.message = message;
        }
    }
}
