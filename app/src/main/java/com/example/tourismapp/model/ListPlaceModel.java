package com.example.tourismapp.model;

import android.util.Log;

import com.example.tourismapp.contracts.ListPlace;
import com.example.tourismapp.network.RetrofitInstance;
import com.example.tourismapp.network.service.GetTourismService;
import com.example.tourismapp.pojos.Place;
import com.example.tourismapp.pojos.PlaceResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListPlaceModel implements ListPlace.Model {
    private final String TAG = "ListPlaceModel";

    @Override
    public void getPlacesList(OnFinishedListener onFinishedListener, String filter, String searchBy) {
        GetTourismService service = RetrofitInstance.getRetrofitInstance().create(GetTourismService.class);
        Call<PlaceResult> call;

        if (searchBy.trim().equals("category")) {
            call = service.getPlacesByCategory(filter);
        } else {
            call = service.getPlaces(filter);
        }

        call.enqueue(new Callback<PlaceResult>() {
            @Override
            public void onResponse(Call<PlaceResult> call, Response<PlaceResult> response) {
                List<Place> placeList = response.body().getPlaces();
                Log.i(TAG, "Registros obtenidos: " + placeList.size());
                onFinishedListener.onFinished(placeList);
            }

            @Override
            public void onFailure(Call<PlaceResult> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                onFinishedListener.onFailure(t);
            }
        });
    }
}
