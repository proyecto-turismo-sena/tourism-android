package com.example.tourismapp.model;

import com.example.tourismapp.contracts.IDeletePlace;
import com.example.tourismapp.network.RetrofitInstance;
import com.example.tourismapp.network.service.GetTourismService;
import com.example.tourismapp.pojos.Place;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeletePlaceModel implements IDeletePlace.Model {
    @Override
    public void deletePlace(OnFinishedListener onFinishedListener, String id) {
        GetTourismService service = RetrofitInstance.getRetrofitInstance().create(GetTourismService.class);
        Call<Place> call;

        call = service.deletePlace(id);
        call.enqueue(new Callback<Place>() {
            @Override
            public void onResponse(Call<Place> call, Response<Place> response) {
                if (response.code() == 200) {
                    onFinishedListener.onFinished();
                }
            }

            @Override
            public void onFailure(Call<Place> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
