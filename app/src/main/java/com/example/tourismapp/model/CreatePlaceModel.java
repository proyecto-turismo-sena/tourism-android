package com.example.tourismapp.model;

import android.net.Uri;

import com.example.tourismapp.contracts.INewPlace;
import com.example.tourismapp.network.RetrofitInstance;
import com.example.tourismapp.network.service.GetTourismService;
import com.example.tourismapp.pojos.Place;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreatePlaceModel implements INewPlace.Model {
    @Override
    public void createPlace(OnFinishedListener onFinishedListener, String name, String address, String website, String email, String mobile, String phone, String category, String photo) {
        MultipartBody.Part image = null;
        if (!photo.trim().equals("")) {
            Uri fileUri = Uri.parse(photo);

            File file = new File(photo);

            RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
            image = MultipartBody.Part.createFormData("image1", file.getName(), requestBody);
        }

        RequestBody placeName = RequestBody.create(MultipartBody.FORM, name);
        RequestBody placeAddress = RequestBody.create(MediaType.parse("text/plain"), address);
        RequestBody placeWebsite = RequestBody.create(MultipartBody.FORM, website);
        RequestBody placeEmail = RequestBody.create(MultipartBody.FORM, email);
        RequestBody placeMobile = RequestBody.create(MediaType.parse("text/plain"), mobile);
        RequestBody placePhone = RequestBody.create(MultipartBody.FORM, phone);
        RequestBody placeCategory = RequestBody.create(MediaType.parse("text/plain"), category);

        GetTourismService service = RetrofitInstance.getRetrofitInstance().create(GetTourismService.class);
        Call<Place> call;
        call = service.createPlace(placeName, placeAddress, placeWebsite, placeEmail, placeMobile, placePhone, placeCategory, image);
        call.enqueue(new Callback<Place>() {
            @Override
            public void onResponse(Call<Place> call, Response<Place> response) {

                if (response.code() == 200) {
                    onFinishedListener.onFinished();
                }
            }

            @Override
            public void onFailure(Call<Place> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
