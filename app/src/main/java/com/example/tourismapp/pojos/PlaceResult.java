package com.example.tourismapp.pojos;

import com.example.tourismapp.pojos.Place;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlaceResult {
    @SerializedName("result")
    private Boolean result;
    @SerializedName("places")
    private List<Place> places;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }
}
