package com.example.tourismapp.pojos;

import com.google.gson.annotations.SerializedName;

public class Place {
    @SerializedName("_id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("address")
    private String address;
    @SerializedName("website")
    private String website;
    @SerializedName("phone")
    private String phone;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile_phone")
    private String mobile_phone;
    @SerializedName("category_id")
    private String category_id;
    @SerializedName("category")
    private Category category;
    @SerializedName("picture1")
    private String image;

    public Place() {
    }

    public Place(String name, String address, String website, String phone, String email, String mobile_phone, String category_id, Category category) {
        this.name = name;
        this.address = address;
        this.website = website;
        this.phone = phone;
        this.email = email;
        this.mobile_phone = mobile_phone;
        this.category_id = category_id;
        this.category = category;
    }

    public Place(String id, String name, String address, String website, String phone, String email, String mobile_phone, String category_id, Category category) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.website = website;
        this.phone = phone;
        this.email = email;
        this.mobile_phone = mobile_phone;
        this.category_id = category_id;
        this.category = category;
    }

    public Place(String id, String name, String address, String website, String phone, String email, String mobile_phone, String category_id, Category category, String image) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.website = website;
        this.phone = phone;
        this.email = email;
        this.mobile_phone = mobile_phone;
        this.category_id = category_id;
        this.category = category;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
