package com.example.tourismapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tourismapp.R;
import com.example.tourismapp.pojos.Place;

import java.util.List;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ViewHolder> {
    public interface OnItemClickListener {
        void onItemClick(Place place);
    }

    private List<Place> mPlaces;
    private OnItemClickListener listener;

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        TextView txtCategory;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txtPlaceName);
            txtCategory = itemView.findViewById(R.id.txtPlaceCategory);
        }

        public void bind(final Place place, final OnItemClickListener listener) {
            txtName.setText(place.getName());
            txtCategory.setText(place.getWebsite());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(place);
                }
            });
        }
    }

    public PlaceAdapter(List<Place> dataPlace, OnItemClickListener listener) {
        mPlaces = dataPlace;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PlaceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.place_element, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PlaceAdapter.ViewHolder holder, int position) {
        Place place = mPlaces.get(position);

        holder.bind(place, listener);
    }

    @Override
    public int getItemCount() {
        return mPlaces.size();
    }
}
