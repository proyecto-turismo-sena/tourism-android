package com.example.tourismapp.base;

import android.app.Application;

import com.example.tourismapp.network.RetrofitInstance;

public class BaseApplication extends Application {
    public static RetrofitInstance retrofitInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        //retrofitInstance = RetrofitInstance.getRetrofitInstance();
    }
}
