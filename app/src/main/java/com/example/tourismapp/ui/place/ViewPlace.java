package com.example.tourismapp.ui.place;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.tourismapp.R;
import com.example.tourismapp.configuration.Config;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewPlace extends AppCompatActivity {
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.tvNamePlace)
    TextView tvName;
    @BindView(R.id.tvAddressPlace)
    TextView tvAddress;
    @BindView(R.id.tvWebsitePlace)
    TextView tvWebsite;
    @BindView(R.id.tvEmailPlace)
    TextView tvEmail;
    @BindView(R.id.tvPhonePlace)
    TextView tvPhone;
    @BindView(R.id.tvMobilePlace)
    TextView tvMobile;

    private String action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_place);

        ButterKnife.bind(this);

        Intent it = getIntent();
        Bundle bundle = getIntent().getExtras();
        Bundle b = bundle.getBundle("typeChoose");
        action = it.getStringExtra("action");

        String imageName = bundle.getString("picturePlace");
        StringBuilder imagePath = new StringBuilder(Config.BASE_URL);
        if (imageName != null) {
            imagePath.append(String.format("/%s/%s", Config.IMAGE_POINT, imageName));
        } else {
            imagePath.append(String.format("/%s/image-not-found.png", Config.IMAGE_POINT));
        }

        tvName.setText(bundle.getString("namePlace"));
        tvAddress.setText(bundle.getString("addressPlace"));
        tvWebsite.setText(bundle.getString("websitePlace"));
        tvEmail.setText(bundle.getString("emailPlace"));
        tvPhone.setText(bundle.getString("phonePlace"));
        tvMobile.setText(bundle.getString("mobilePlace"));
        Picasso.get().load(imagePath.toString()).fit().into(imageView);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ListPlaces.class).putExtra("typeChoose", b).putExtra("action", action));
                finish();
            }
        });
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @OnClick({R.id.ibPhonePlace, R.id.ibMobilePlace})
    public void makeCall(View view) {
        String numberToCall = "";
        switch (view.getId()) {
            case R.id.ibPhonePlace:
                numberToCall = tvPhone.getText().toString();
                break;
            case R.id.ibMobilePlace:
                numberToCall = tvMobile.getText().toString();
                break;
            default:
                break;
        }

        if (!numberToCall.trim().equals("")) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + numberToCall));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }

    @OnClick(R.id.ibWebsitePlace)
    public void broseWebsite(View view) {
        String webapp = tvWebsite.getText().toString();

        if (!webapp.trim().equals("")) {
            Uri webpage = Uri.parse("http:" + webapp);
            Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }

    @OnClick(R.id.ibEmailPlace)
    public void sendEmail(View view) {
        String sendTo = tvEmail.getText().toString();

        if (!sendTo.trim().equals("")) {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, sendTo);
            intent.putExtra(Intent.EXTRA_SUBJECT, "Mail to place");
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }
}
