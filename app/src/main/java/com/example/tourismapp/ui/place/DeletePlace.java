package com.example.tourismapp.ui.place;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.tourismapp.configuration.Config;
import com.example.tourismapp.contracts.IDeletePlace;
import com.example.tourismapp.event.Events;
import com.example.tourismapp.presenter.DeletePlacePresenter;
import com.example.tourismapp.ui.main.OperationsSite;
import com.example.tourismapp.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeletePlace extends AppCompatActivity implements IDeletePlace.View {
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.tvNamePlace)
    TextView tvName;
    @BindView(R.id.tvAddressPlace)
    TextView tvAddress;
    @BindView(R.id.tvWebsitePlace)
    TextView tvWebsite;
    @BindView(R.id.tvEmailPlace)
    TextView tvEmail;
    @BindView(R.id.tvPhonePlace)
    TextView tvPhone;
    @BindView(R.id.tvMobilePlace)
    TextView tvMobile;
    @BindView(R.id.progressBar)
    View progressBar;

    private Bundle b;
    private String action;
    private String idPlace;
    private DeletePlacePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_place);

        ButterKnife.bind(this);

        Intent it = getIntent();
        Bundle bundle = getIntent().getExtras();
        b = it.getBundleExtra("typeChoose");
        String type = b.getString("type");
        action = it.getStringExtra("action");

        presenter = new DeletePlacePresenter(this);
        presenter.setValues(bundle);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DeletePlace.this, OperationsSite.class).putExtra("typeChoose", b).putExtra("action", action));
                finish();
            }
        });
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.btnDelete)
    public void deletePlace(View view) {
        //String namePlace = editNamePlace.getText().toString();

        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppThemeDialog));
        dialog.setTitle(getString(R.string.txt_delete));
        dialog.setMessage(getString(R.string.txt_message_delete));
        dialog.setCancelable(true);
        dialog.setPositiveButton(getString(R.string.dialog_accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.deletePlace(idPlace);
            }
        });
        dialog.setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void setValues(Bundle bundle) {
        idPlace = bundle.getString("idPlace");

        String imageName = bundle.getString("picturePlace");
        StringBuilder imagePath = new StringBuilder(Config.BASE_URL);
        if (imageName != null) {
            imagePath.append(String.format("/%s/%s", Config.IMAGE_POINT, imageName));
        } else {
            imagePath.append(String.format("/%s/image-not-found.png", Config.IMAGE_POINT));
        }

        tvName.setText(bundle.getString("namePlace"));
        tvAddress.setText(bundle.getString("addressPlace"));
        tvWebsite.setText(bundle.getString("websitePlace"));
        tvEmail.setText(bundle.getString("emailPlace"));
        tvPhone.setText(bundle.getString("phonePlace"));
        tvMobile.setText(bundle.getString("mobilePlace"));
        Picasso.get().load(imagePath.toString()).fit().into(imageView);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void successDeleteResult() {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppThemeDialog));
        builder.setTitle(getString(R.string.dialog_title_success_delete));
        builder.setMessage(getString(R.string.dialog_message_success_delete));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(getApplicationContext(), ListPlaces.class).putExtra("typeChoose", b).putExtra("action", action));
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void errorDeleteResult() {
        String title = getString(R.string.dialog_title_error);
        String message = getString(R.string.dialog_message_error);

        EventBus.getDefault().post(new Events.DialogErrorEvent(title, message));
    }
}
