package com.example.tourismapp.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.tourismapp.R;
import com.example.tourismapp.configuration.Config;
import com.example.tourismapp.ui.place.ListPlaces;
import com.example.tourismapp.ui.place.NewPlace;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OperationsSite extends AppCompatActivity {
    @BindView(R.id.txtTitle) TextView txtTitle;
    @BindView(R.id.imCategory)
    ImageView imgCategory;

    private Bundle b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operations_site);

        ButterKnife.bind(this);

        Intent it = getIntent();
        b = it.getBundleExtra("typeChoose");
        String type = b.getString("type");
        String category = b.getString("categoryId");

        txtTitle.setText(type);

        if (category.equals(Config.ID_CATEGORY_ARC)) {
            Picasso.get().load(R.drawable.arqueologia).fit().into(imgCategory);
        } else if (category.equals(Config.ID_CATEGORY_HOTELS)) {
            Picasso.get().load(R.drawable.imagen_piesenhotel).fit().into(imgCategory);
        } else if (category.equals(Config.ID_CATEGORY_PARK)) {
            Picasso.get().load(R.drawable.natural_park).fit().into(imgCategory);
        }

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OperationsSite.this, MainActivity.class));
                finish();
            }
        });
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @OnClick({R.id.btnList, R.id.btnUpdate, R.id.btnDelete})
    public void getListPlace(View view) {
        Intent intent = new Intent(this, ListPlaces.class);
        intent.putExtra("typeChoose", b);

        switch (view.getId()) {
            case R.id.btnList:
                intent.putExtra("action", Config.ACTION_WATCH);
                break;
            case R.id.btnUpdate:
                intent.putExtra("action", Config.ACTION_UPDATE);
                break;
            case R.id.btnDelete:
                intent.putExtra("action", Config.ACTION_DELETE);
                break;
        }

        startActivity(intent);
    }

    @OnClick(R.id.btnAdd)
    public void addNewPlace(View view) {
        startActivity(new Intent(OperationsSite.this, NewPlace.class).putExtra("typeChoose", b));
    }
}
