package com.example.tourismapp.ui.place;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.tourismapp.R;
import com.example.tourismapp.contracts.IUpdatePlace;
import com.example.tourismapp.contracts.TakePictureFragment;
import com.example.tourismapp.event.Events;
import com.example.tourismapp.presenter.UpdatePlacePresenter;
import com.example.tourismapp.ui.main.OperationsSite;
import com.example.tourismapp.ui.picture.TakePicture;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdatePlace extends AppCompatActivity implements IUpdatePlace.View, TakePicture.OnFragmentInteractionListener, TakePictureFragment.DialogListener {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.txt_name_place)
    TextInputLayout layoutNamePlace;
    @BindView(R.id.edit_name_place)
    TextInputEditText editNamePlace;
    @BindView(R.id.txt_address_place)
    TextInputLayout layoutAddressPlace;
    @BindView(R.id.edit_address_place)
    TextInputEditText editAddressPlace;
    @BindView(R.id.txt_website_place)
    TextInputLayout layoutWebsitePlace;
    @BindView(R.id.edit_website_place)
    TextInputEditText editWebsitePlace;
    @BindView(R.id.txt_email_place)
    TextInputLayout layoutEmailPlace;
    @BindView(R.id.edit_email_place)
    TextInputEditText editEmailPlace;
    @BindView(R.id.txt_phone_place)
    TextInputLayout layoutPhonePlace;
    @BindView(R.id.edit_phone_place)
    TextInputEditText editPhonePlace;
    @BindView(R.id.txt_mobile_place)
    TextInputLayout layoutMobilePlace;
    @BindView(R.id.edit_mobile_place)
    TextInputEditText editMobilePlace;
    @BindView(R.id.edit_picture_place)
    TextInputEditText editPicturePlace;
    @BindView(R.id.progressBar)
    View progressBar;

    private Bundle b;
    private String action;
    private String idPlace;
    private String category;
    private TakePicture takePicture;
    private UpdatePlacePresenter placePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_place);

        ButterKnife.bind(this);

        Intent it = getIntent();
        Bundle bundle = getIntent().getExtras();
        b = it.getBundleExtra("typeChoose");
        String type = b.getString("type");
        category = b.getString("categoryId");
        action = it.getStringExtra("action");

        String newPlace = "";
        if (type.equals(getString(R.string.txt_turistics_sites))) {
            newPlace = "sitio turistico";
        } else if (type.equals(getString(R.string.txt_hotels))) {
            newPlace = "hotel";
        } else if (type.equals(getString(R.string.txt_turistics_operators))) {
            newPlace = "operador turistico";
        }

        txtTitle.setText(String.format("Modificar %s", newPlace));

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UpdatePlace.this, OperationsSite.class).putExtra("typeChoose", b).putExtra("action", action));
                finish();
            }
        });
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        setValues(bundle);
        takePicture = new TakePicture();

        placePresenter = new UpdatePlacePresenter(this);
    }

    @OnClick(R.id.ibCamera)
    public void takePicture(View view) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("fullScreen", true);
        bundle.putBoolean("notAlertDialog", true);

        takePicture.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            transaction.remove(prev);
        }
        transaction.addToBackStack(null);

        takePicture.show(transaction, "dialog");
    }

    @OnClick(R.id.btnUpdate)
    public void updatePlace(View view) {
        String namePlace = editNamePlace.getText().toString();
        String addressPlace = editAddressPlace.getText().toString();
        String websitePlace = editWebsitePlace.getText().toString();
        String emailPlace = editEmailPlace.getText().toString();
        String phonePlace = editPhonePlace.getText().toString();
        String mobilePlace = editMobilePlace.getText().toString();
        String pictureLocation = editPicturePlace.getText().toString();

        if (namePlace.trim().equals("")) {
            layoutNamePlace.setError(getString(R.string.err_message_place_name));
            editNamePlace.requestFocus();
        } else {
            layoutNamePlace.setErrorEnabled(false);
        }

        if (addressPlace.trim().equals("")) {
            layoutAddressPlace.setError(getString(R.string.err_message_place_address));
            editAddressPlace.requestFocus();
        } else {
            layoutAddressPlace.setErrorEnabled(false);
        }

        if (websitePlace.trim().equals("")) {
            layoutWebsitePlace.setError(getString(R.string.err_message_place_website));
            editWebsitePlace.requestFocus();
        } else {
            layoutWebsitePlace.setErrorEnabled(false);
        }

        if (emailPlace.trim().equals("")) {
            layoutEmailPlace.setError(getString(R.string.err_message_place_email));
            editEmailPlace.requestFocus();
        } else {
            layoutEmailPlace.setErrorEnabled(false);
        }

        if (phonePlace.trim().equals("")) {
            layoutPhonePlace.setError(getString(R.string.err_message_place_phone));
            editPhonePlace.requestFocus();
        } else {
            layoutPhonePlace.setErrorEnabled(false);
        }

        if (mobilePlace.trim().equals("")) {
            layoutMobilePlace.setError(getString(R.string.err_message_place_mobile));
            editMobilePlace.requestFocus();
        } else {
            layoutMobilePlace.setErrorEnabled(false);
        }

        placePresenter.updatePlace(idPlace, namePlace, addressPlace, websitePlace, emailPlace, mobilePlace, phonePlace, category, pictureLocation);
    }

    @Override
    public void setValues(Bundle bundle) {
        idPlace = bundle.getString("idPlace");
        editNamePlace.setText(bundle.getString("namePlace"));
        editAddressPlace.setText(bundle.getString("addressPlace"));
        editWebsitePlace.setText(bundle.getString("websitePlace"));
        editEmailPlace.setText(bundle.getString("emailPlace"));
        editPhonePlace.setText(bundle.getString("phonePlace"));
        editMobilePlace.setText(bundle.getString("mobilePlace"));
    }

    @Override
    public void clearFields() {
        editNamePlace.setText("");
        editAddressPlace.setText("");
        editWebsitePlace.setText("");
        editEmailPlace.setText("");
        editPhonePlace.setText("");
        editMobilePlace.setText("");
        editPicturePlace.setText("");
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void successUpdatedResult() {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppThemeDialog));
        builder.setTitle(getString(R.string.dialog_title_success_register));
        builder.setMessage(String.format(getString(R.string.dialog_message_success_update), editNamePlace.getText().toString()));
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(getApplicationContext(), ListPlaces.class).putExtra("typeChoose", b).putExtra("action", action));
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void errorUpdatedResult() {
        String title = getString(R.string.dialog_title_error);
        String message = getString(R.string.dialog_message_error);

        EventBus.getDefault().post(new Events.DialogErrorEvent(title, message));
    }

    @Override
    public void onFinishEditDialog(String inputText) {
        editPicturePlace.setText(inputText);
        takePicture.dismiss();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
