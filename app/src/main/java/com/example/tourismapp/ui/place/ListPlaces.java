package com.example.tourismapp.ui.place;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tourismapp.R;
import com.example.tourismapp.adapter.PlaceAdapter;
import com.example.tourismapp.base.BaseActivity;
import com.example.tourismapp.configuration.Config;
import com.example.tourismapp.contracts.ListPlace;
import com.example.tourismapp.event.Events;
import com.example.tourismapp.pojos.Place;
import com.example.tourismapp.presenter.PlaceListPresenter;
import com.example.tourismapp.ui.main.OperationsSite;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListPlaces extends BaseActivity implements ListPlace.View {
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.searchView)
    SearchView search;
    @BindView(R.id.listPlaces)
    RecyclerView recyclerPlace;
    @BindView(R.id.txtNotFound)
    TextView txtNotRecords;
    @BindView(R.id.progressBar)
    View progressBar;

    private Bundle b;
    private String action;
    PlaceAdapter adapter;
    List<Place> placeList;
    private PlaceListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_places);

        ButterKnife.bind(this);

        // Captura de los datos enviados desde la actividad anterior.
        Intent it = getIntent();
        b = it.getBundleExtra("typeChoose");
        String type = b.getString("type");
        String category = b.getString("categoryId");
        action = it.getStringExtra("action");

        txtTitle.setText(type);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListPlaces.this, OperationsSite.class).putExtra("typeChoose", b));
                finish();
            }
        });
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        search.setQueryHint(String.format("Buscar %s", type.toLowerCase()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.wtf("SEARC", "text change" + newText);
                doSearchPlaces(newText, "name");
                return false;
            }
        });

        initView();

        presenter = new PlaceListPresenter(this);

        doSearchPlaces(category, "category");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    /**
     * Inicia el recyclerview donde se mostraran los sitios.
     */
    private void initView() {
        placeList = new ArrayList<>();

        adapter = new PlaceAdapter(placeList, new PlaceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Place place) {

                Intent intent = null;

                Bundle bundle = new Bundle();
                bundle.putString("idPlace", place.getId());
                bundle.putString("namePlace", place.getName());
                bundle.putString("addressPlace", place.getAddress());
                bundle.putString("websitePlace", place.getWebsite());
                bundle.putString("emailPlace", place.getEmail());
                bundle.putString("phonePlace", place.getPhone());
                bundle.putString("mobilePlace", place.getMobile_phone());
                bundle.putString("picturePlace", place.getImage());
                bundle.putBundle("typeChoose", b);

                if (action.equals(Config.ACTION_WATCH)) {
                    intent = new Intent(getApplicationContext(), ViewPlace.class);
                    intent.putExtra("action", Config.ACTION_WATCH);
                } else if (action.equals(Config.ACTION_UPDATE)) {
                    intent = new Intent(getApplicationContext(), UpdatePlace.class);
                    intent.putExtra("action", Config.ACTION_UPDATE);
                } else if (action.equals(Config.ACTION_DELETE)) {
                    intent = new Intent(getApplicationContext(), DeletePlace.class);
                    intent.putExtra("action", Config.ACTION_DELETE);
                }

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerPlace.setLayoutManager(layoutManager);
        recyclerPlace.setAdapter(adapter);
    }

    /**
     * Invoca el metodo que realiza la consulta de los sitios.
     *
     * @param filter Valor por el cual se realizara la busquedad.
     * @param searchBy Indica sobre que campo se realizara la busquedad.
     */
    private void doSearchPlaces(String filter, String searchBy) {
        presenter.requestData(filter, searchBy);
    }

    @Override
    public void showListPlaces() {
        recyclerPlace.setVisibility(View.VISIBLE);
        txtNotRecords.setVisibility(View.GONE);
    }

    @Override
    public void hideListPlaces() {
        recyclerPlace.setVisibility(View.GONE);
        txtNotRecords.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setDataAdapter(List<Place> places) {
        this.placeList.addAll(places);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        String title = getString(R.string.dialog_title_search);
        String message = getString(R.string.dialog_message_search_error);

        EventBus.getDefault().post(new Events.DialogErrorEvent(title, message));
    }
}
