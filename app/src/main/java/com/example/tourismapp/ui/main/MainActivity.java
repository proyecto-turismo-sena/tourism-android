package com.example.tourismapp.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.tourismapp.R;
import com.example.tourismapp.configuration.Config;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private final String SITES = Config.ID_CATEGORY_ARC;
    private final String HOTELS = Config.ID_CATEGORY_HOTELS;
    private final String OPERATORS = Config.ID_CATEGORY_PARK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
    }

    @OnClick({R.id.btnSites, R.id.btnOperators, R.id.btnHotels})
    public void choosedType(View view) {
        String strTitle = ((Button)view).getText().toString();
        Intent intent = new Intent(this, OperationsSite.class);
        Bundle bundle = new Bundle();

        switch (view.getId()) {
            case R.id.btnSites:
                bundle.putString("categoryId", SITES);
                break;
            case R.id.btnHotels:
                bundle.putString("categoryId", HOTELS);
                break;
            case R.id.btnOperators:
                bundle.putString("categoryId", OPERATORS);
                break;
            default:
                break;
        }
        bundle.putString("type", strTitle);
        intent.putExtra("typeChoose", bundle);
        startActivity(intent);
    }
}
