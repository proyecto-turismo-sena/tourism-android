package com.example.tourismapp.network.service;

import com.example.tourismapp.pojos.Place;
import com.example.tourismapp.pojos.PlaceResult;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface GetTourismService {
    @GET("/places")
    Call<PlaceResult> getAllPlaces();
    //Call<List<Place>> getAllPlaces();

    @GET("/places/{name}")
    Call<PlaceResult> getPlaces(@Path("name") String name);

    @GET("/places/category/{category}")
    Call<PlaceResult> getPlacesByCategory(@Path("category") String category);

    //@FormUrlEncoded
    //@POST("/place")
    //Call<Place> createPlace(@Field("name") String name, @Field("address") String address, @Field("website") String website, @Field("mobile") String mobile, @Field("phone") String phone, @Field("category") String category);
    //Call<Place> createPlaces(@Body Place place);
    @Multipart
    @POST("/place")
    Call<Place> createPlace(
            @Part("name") RequestBody name,
            @Part("address") RequestBody address,
            @Part("website") RequestBody website,
            @Part("mobile") RequestBody mobile,
            @Part("email") RequestBody email,
            @Part("phone") RequestBody phone,
            @Part("category") RequestBody category,
            @Part MultipartBody.Part image
    );

    @Multipart
    @PUT("/place/{id}")
    Call<Place> updatePlace(
            @Path("id") String id,
            @Part("name") RequestBody name,
            @Part("address") RequestBody address,
            @Part("website") RequestBody website,
            @Part("email") RequestBody email,
            @Part("mobile_phone") RequestBody mobile,
            @Part("phone") RequestBody phone,
            @Part("category") RequestBody category,
            @Part MultipartBody.Part image
    );

    @DELETE("/place/{id}")
    Call<Place> deletePlace(@Path("id") String id);
}
