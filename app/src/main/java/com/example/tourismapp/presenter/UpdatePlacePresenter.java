package com.example.tourismapp.presenter;

import com.example.tourismapp.contracts.IUpdatePlace;
import com.example.tourismapp.model.UpdatePlaceModel;

public class UpdatePlacePresenter implements IUpdatePlace.Presenter, IUpdatePlace.Model.OnFinishedListener {
    private IUpdatePlace.View view;
    private IUpdatePlace.Model model;

    public UpdatePlacePresenter(IUpdatePlace.View view) {
        this.view = view;
        model = new UpdatePlaceModel();
    }

    @Override
    public void doClear() {
        view.clearFields();
    }

    @Override
    public void updatePlace(String id, String name, String address, String website, String email, String mobile, String phone, String category, String photo) {
        if (this.view != null) {
            this.view.showProgressBar();
        }
        model.updatePlace(this, id, name, address, website, email, mobile, phone, category, photo);
    }

    @Override
    public void onFinished() {
        if (this.view != null) {
            this.view.hideProgressBar();
            this.view.successUpdatedResult();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (this.view != null) {
            this.view.hideProgressBar();
            view.errorUpdatedResult();
        }
    }
}
