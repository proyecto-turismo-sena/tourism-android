package com.example.tourismapp.presenter;

import android.os.Bundle;

import com.example.tourismapp.contracts.IDeletePlace;
import com.example.tourismapp.model.DeletePlaceModel;

public class DeletePlacePresenter implements IDeletePlace.Presenter, IDeletePlace.Model.OnFinishedListener {
    private IDeletePlace.View view;
    private IDeletePlace.Model model;

    public DeletePlacePresenter(IDeletePlace.View view) {
        this.view = view;
        this.model = new DeletePlaceModel();
    }

    @Override
    public void setValues(Bundle bundle) {
        if (view != null) {
            view.setValues(bundle);
        }
    }

    @Override
    public void deletePlace(String id) {
        if (view != null) {
            view.showProgressBar();
        }

        model.deletePlace(this, id);
    }

    @Override
    public void onFinished() {
        if (view != null) {
            view.hideProgressBar();
            view.successDeleteResult();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (view != null) {
            view.hideProgressBar();
            view.errorDeleteResult();
        }
    }
}
