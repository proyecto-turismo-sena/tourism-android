package com.example.tourismapp.presenter;

import com.example.tourismapp.contracts.INewPlace;
import com.example.tourismapp.model.CreatePlaceModel;

public class NewPlacePresenter implements INewPlace.Presenter, INewPlace.Model.OnFinishedListener {
    private INewPlace.View newPlaceView;
    private INewPlace.Model newPlaceModel;

    public NewPlacePresenter(INewPlace.View newPlaceView) {
        this.newPlaceView = newPlaceView;
        newPlaceModel = new CreatePlaceModel();
    }

    @Override
    public void doClear() {
        newPlaceView.clearFields();
    }

    @Override
    public void registerPlace(String name, String address, String website, String email, String mobile, String phone, String category, String photo) {
        if (newPlaceView != null) {
            newPlaceView.showProgressBar();
        }

        newPlaceModel.createPlace(this, name, address, website, email, mobile, phone, category, photo);
    }

    @Override
    public void onFinished() {
        if (newPlaceView != null) {
            newPlaceView.hideProgressBar();
            newPlaceView.successRegisterResult();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if (newPlaceView != null) {
            newPlaceView.hideProgressBar();
            newPlaceView.errorRegisterResult();
        }
    }
}
