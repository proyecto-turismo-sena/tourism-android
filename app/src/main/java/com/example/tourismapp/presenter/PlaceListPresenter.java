package com.example.tourismapp.presenter;

import com.example.tourismapp.contracts.ListPlace;
import com.example.tourismapp.model.ListPlaceModel;
import com.example.tourismapp.pojos.Place;

import java.util.List;

public class PlaceListPresenter implements ListPlace.Presenter, ListPlace.Model.OnFinishedListener {
    private ListPlace.View placeListView;
    private ListPlace.Model placeListModel;

    public PlaceListPresenter(ListPlace.View placeListView) {
        this.placeListView = placeListView;
        placeListModel = new ListPlaceModel();
    }

    @Override
    public void onDestroy() {
        placeListView = null;
    }

    @Override
    public void requestData(String filter, String searchBy) {
        if (placeListView != null) {
            placeListView.showProgressBar();
        }
        placeListModel.getPlacesList(this, filter, searchBy);
    }

    @Override
    public void onFinished(List<Place> placeList) {
        placeListView.setDataAdapter(placeList);
        if (placeListView != null) {
            placeListView.hideProgressBar();
            placeListView.showListPlaces();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        placeListView.onResponseFailure(t);
        if (placeListView != null) {
            placeListView.hideProgressBar();
            placeListView.hideListPlaces();
        }
    }
}
