package com.example.tourismapp.configuration;

public class Config {
    public static final String BASE_URL = "https://alejosaag-tourismws.herokuapp.com";
    public static final String IMAGE_POINT = "image";
    public static final String ACTION_WATCH = "watch";
    public static final String ACTION_UPDATE = "update";
    public static final String ACTION_DELETE = "delete";

    public static final String ID_CATEGORY_HOTELS = "5d26a129174e0d00178ba111";
    public static final String ID_CATEGORY_ARC = "5d60c91413763a0017c36deb";
    public static final String ID_CATEGORY_PARK = "5d60ca5913763a0017c36dec";
}
