package com.example.tourismapp.contracts;

public interface TakePictureFragment {


    public interface DialogListener {
        void onFinishEditDialog(String inputText);
    }
}
