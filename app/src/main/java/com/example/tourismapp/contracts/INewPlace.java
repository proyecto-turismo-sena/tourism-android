package com.example.tourismapp.contracts;

public interface INewPlace {
    interface View {
        void clearFields();
        void showProgressBar();

        void hideProgressBar();
        void successRegisterResult();
        void errorRegisterResult();
    }

    interface Presenter {
        void doClear();
        void registerPlace(String name, String address, String website, String email, String mobile, String phone, String category, String photo);
    }

    interface Model {
        interface OnFinishedListener {
            void onFinished();
            void onFailure(Throwable t);
        }

        void createPlace(OnFinishedListener onFinishedListener, String name, String address, String website, String email, String mobile, String phone, String category, String photo);
    }
}
