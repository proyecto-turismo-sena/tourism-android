package com.example.tourismapp.contracts;

import android.os.Bundle;

public interface IUpdatePlace {
    interface View {
        void setValues(Bundle bundle);
        void clearFields();

        void showProgressBar();

        void hideProgressBar();
        void successUpdatedResult();
        void errorUpdatedResult();
    }

    interface Presenter {
        void doClear();
        void updatePlace(String id, String name, String address, String website, String email, String mobile, String phone, String category, String photo);
    }

    interface Model {
        interface OnFinishedListener {
            void onFinished();

            void onFailure(Throwable t);
        }

        void updatePlace(OnFinishedListener onFinishedListener, String id, String name, String address, String website, String email, String mobile, String phone, String category, String photo);
    }
}
