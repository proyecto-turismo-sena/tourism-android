package com.example.tourismapp.contracts;

import com.example.tourismapp.pojos.Place;

import java.util.List;

public interface ListPlace {
    interface View {
        void showListPlaces();
        void hideListPlaces();
        void showProgressBar();
        void hideProgressBar();
        void setDataAdapter(List<Place> placeList);
        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();
        void requestData(String filter, String searchBy);
    }

    interface Model {
        interface OnFinishedListener {
            void onFinished(List<Place> placeList);
            void onFailure(Throwable t);
        }
        void getPlacesList(OnFinishedListener onFinishedListener, String filter, String searchBy);
    }
}
