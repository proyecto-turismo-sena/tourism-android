package com.example.tourismapp.contracts;

import android.os.Bundle;

public interface IDeletePlace {
    interface View {
        void setValues(Bundle bundle);

        void showProgressBar();

        void hideProgressBar();

        void successDeleteResult();

        void errorDeleteResult();
    }

    interface Presenter {
        void setValues(Bundle bundle);
        void deletePlace(String id);
    }

    interface Model {
        interface OnFinishedListener {
            void onFinished();

            void onFailure(Throwable t);
        }

        void deletePlace(OnFinishedListener onFinishedListener, String id);
    }
}
